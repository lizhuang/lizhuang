function showArticles() {
  var grid = new Minigrid({
    container: '.grid',
    item: '.grid-item',
    gutter: 12
  });
  grid.mount();
  $(window).resize(function () {
    grid.mount();
  });
}


var hashCode = function (str) {
  if (!str && str.length === 0) {
    return 0;
  }

  var hash = 0;
  for (var i = 0, len = str.length; i < len; i++) {
    hash = ((hash << 5) - hash) + str.charCodeAt(i);
    hash |= 0;
  }
  return hash;
};

/* 卡片文章缩略图随机 */
function showImages() {
  var len = 23;
  $(".card-image img").each(function () {
    var title = ($(this).attr("data-title"));
    if (title) {
      var num = Math.abs(hashCode(title) % len);
      var image = '/images/featureimages/' + num + '.jpg';
      $(this).attr("src", image);
    }
  });
}

$(document).ready(function () {
  showArticles();
  showImages();
});
